var gulp = require('gulp');
var del = require('del');
var browserSync = require('browser-sync').create();
var imagemin = require('gulp-imagemin');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

// Directorios de origen
var srcPaths = {
    images: 'src/img/',
    scripts: 'src/js/',
    styles: 'src/scss/',
    vendor: 'src/vendor/',
    data: 'src/data/',
    files: 'src/'
};


// Directorios de destino
var distPaths = {
    images: 'dist/img/',
    scripts: 'dist/js/',
    styles: 'dist/css/',
    vendor: 'dist/vendor/',
    data: 'dist/data/',
    files: 'dist/'
};

// Tarea para limpiar la carpeta dist
gulp.task('clean', function (cb) {
    del([distPaths.files + '*'], cb);
    cb();
});

// Tarea para copiar elementos que no necesitan ser procesados a la carpeta de distribución
gulp.task('copy-html', function () {
    return gulp.src([srcPaths.files + '*.html'])
        .pipe(gulp.dest(distPaths.files))
        .pipe(browserSync.stream());
});

gulp.task('copy-data', function () {
    return gulp.src([srcPaths.data + '**/*'])
        .pipe(gulp.dest(distPaths.data))
        .pipe(browserSync.stream());
});

gulp.task('copy-vendor', function () {
    return gulp.src([srcPaths.vendor + '**/*'])
        .pipe(gulp.dest(distPaths.vendor))
        .pipe(browserSync.stream());
});

gulp.task('copy', gulp.series('copy-html', 'copy-data', 'copy-vendor'));

// Tarea para minificar las imágenes y copiarlas en la carpeta dist
gulp.task('imagemin', function () {
    return gulp.src([srcPaths.images + '**/*'])
        .pipe(imagemin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{ removeUnknownsAndDefaults: false }, { cleanupIDs: false }]
        }))
        .pipe(gulp.dest(distPaths.images))
        .pipe(browserSync.stream());
});

// Tarea para procesar los ficheros sass en un fichero css.
gulp.task('scss', function () {
    return gulp.src([srcPaths.styles + '**/*.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(distPaths.styles))
        .pipe(browserSync.stream());
});

// Tarea para comprobar posibles errores en los ficheros javascript
gulp.task('lint', function () {
    return gulp.src([srcPaths.scripts + '**/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

/* Tarea para generar un único fichero js minificado */
gulp.task('js', gulp.series('lint', function () {
    return gulp.src([srcPaths.scripts + 'contact.js', srcPaths.scripts + 'contactList.js', srcPaths.scripts + 'main.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('all.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(distPaths.scripts))
        .pipe(browserSync.stream());
}));

/* Tarea para lanzar el proceso en el servidor. Para ello es necesario procesar todos los ficheros del src
necesarios para levantar la web */
gulp.task('serve', gulp.series('copy', 'imagemin', 'scss', 'js', function () {
    browserSync.init({
        server: distPaths.files,
        logLevel: "info",
        browser: ["firefox", "chrome"]
    });
    gulp.watch(srcPaths.files + '*.html', gulp.series('copy-html'));
    gulp.watch(srcPaths.images + '**/*', gulp.series('imagemin'));
    gulp.watch(srcPaths.styles + '**/*.scss', gulp.series('scss'));
    gulp.watch(srcPaths.scripts + '**/*.js', gulp.series('js'));
}));

/* Tarea por defecto que limpia la carpeta dist y luego lanza el servidor */
gulp.task('default', gulp.series('clean', 'serve'));

/* Tarea para lanzar la aplicación en el xampp */
gulp.task('serve-xampp', gulp.series('copy', 'imagemin', 'scss', 'js', function () {
    browserSync.init({
        logLevel: "info",
        browser: ["firefox", "chrome"],
        proxy: "localhost:80",
        startPath: '/contacts-agenda/dist/'
    });
    gulp.watch(srcPaths.files + '*.html', gulp.series('copy-html'));
    gulp.watch(srcPaths.images + '**/*', gulp.series('imagemin'));
    gulp.watch(srcPaths.styles + '**/*.scss', gulp.series('scss'));
    gulp.watch(srcPaths.scripts + '**/*.js', gulp.series('js'));
}));

/* Tarea que limpia el dist y lanza el server en el xampp */
gulp.task('xampp', gulp.series('clean', 'serve-xampp'));